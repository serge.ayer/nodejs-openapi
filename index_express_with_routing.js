const express = require('express');
const app = express();

const hostname = '127.0.0.1';
const port = 3000;

let noRouteFound = (req, res) => {
  let route = req.method + ' ' + req.url;
  res.end('You asked for ' + route);
};

let helloWorldRoute = (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.send('Hello World!');
};

app.get('/', helloWorldRoute);
app.get('*', noRouteFound);

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
