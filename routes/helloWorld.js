const express = require('express');
const router = express.Router();

// will handle any request on the root path
// that is relative to where the router is used (path in use())
router.route('/').get((req, res, next ) => {
  // do something
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.send('Hello World!');

  // at this point, the response is sent to the client,
  // so do not call the next middleware
  // next();
});


// Export the router to make it accessible for "requirers" of this file
module.exports = router;