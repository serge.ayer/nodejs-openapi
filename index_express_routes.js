const express = require('express');
const app = express();
const helloWorldRoute = require('./routes/helloWorld');

const hostname = '127.0.0.1';
const port = 3000;

let noRouteFound = (req, res) => {
  let route = req.method + ' ' + req.url;
  res.end('You asked for ' + route);
};

app.use('/', helloWorldRoute);
app.get('*', noRouteFound);

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
