const http = require('http');
const express = require('express');

const hostname = '127.0.0.1';
const port = 3000;

let app = express();

app.use((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello, World!\n');
});

let server = http.createServer(app);
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
